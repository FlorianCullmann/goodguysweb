<?php
if(!isset($_SESSION['ID']))
{
  header("Location: index.php");
}

$query = mysqli_query($db, "SELECT * FROM gg_itemsets WHERE active=1");
while($fetch = mysqli_fetch_object($query))
{
  echo '<h2>'.$fetch->name.'</h2><br />';
  $query2 = mysqli_query($db, "SELECT * FROM gg_items WHERE active=1 AND itemset='".$fetch->ID."'");
  while($fetch2 = mysqli_fetch_object($query2))
  {
    $query3 = mysqli_query($db, "SELECT * FROM gg_items_owned WHERE itemID='".$fetch2->ID."'");
    $query4 = mysqli_query($db, "SELECT * FROM gg_items_owned WHERE ownerID='".$USER['ID']."'");
    $numitemsowned = mysqli_num_rows($query3);
    $numitemsfree = $fetch2->limitation - $numitemsowned;
    $numuserowned = mysqli_num_rows($query4);

    $rarestatus = getRareStatus($fetch2->ID);

    ?>
    <div class="card bg-dark" style="width: 18rem;">
      <img class="card-img-top" src="assets/img/items/<?= $fetch2->image; ?>" alt="image missing.">
      <div class="card-body">
        <h5 class="card-title"><?= $fetch2->name; ?> (<?= $rarestatus; ?>)</h5>
        <p class="card-text"><?= $fetch2->description; ?></p>
        <?php
        if(($numitemsfree <= 0) || ($USER['gc'] < $fetch2->storeprice))
        {
          ?><button class="btn btn-primary" disabled>Kauf nicht möglich</button><?php 
        }
        else
        {
          ?><a href="controller/store.php?cmd=buy&itemID=<?= $fetch2->ID; ?>" class="btn btn-primary"><?= $fetch2->storeprice; ?> GC</a><?php
        }
        ?>
        <br /><?= $numuserowned; ?> im Besitz | <?= $numitemsfree; ?> / <?= $fetch2->limitation; ?> übrig
      </div>
    </div>
    <?php
  }
}
?>