<?php
if(!isset($_SESSION['ID']))
{
  header("Location: index.php?s=login&login=false");
}
if(!isset($_GET['ID']))
{
  $_GET['ID'] = $USER['ID'];
}
$query = mysqli_query($db, "SELECT * FROM gg_users WHERE ID='".$_GET['ID']."'");
$fetch = mysqli_fetch_object($query);
?>
<div class="row">
  <div class="col-md-2 col-xs-12">
    <img width="80" height="80" src="assets/img/profiles/<?= $USER['ID']; ?>.png">
  </div>
  <div class="col-md-10 col-xs-12">
    <h2><?= $fetch->username; ?></h2>
    <h5>Level <?= $fetch->level; ?> | <span style="color:<?= getStatusByID($fetch->status)->color; ?>;"><?= getStatusByID($fetch->status)->name; ?></span></h5>
  </div>
</div>
<?php if($_GET['ID'] !== $USER['ID']) { echo getFriendshipButton($USER['ID'], $_GET['ID']); } ?>
<hr>
<h2>Showcase</h2>