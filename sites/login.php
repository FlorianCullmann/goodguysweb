<?php if((isset($_GET['logout'])) && ($_GET['logout'] == "true")) { ?><div class="alert alert-success">Du wurdest erfolgreich abgemeldet.</div><?php } ?>

<h3>Im Netzwerk anmelden</h3>

<form action="controller/login.php" method="POST">
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">E-Mail Adresse</span>
    </div>
    <input type="email" name="login-email" aria-label="E-Mail Adresse" class="form-control bg-dark text-white">
  </div>
  <br />
  <div class="input-group">
    <div class="input-group-prepend">
      <span class="input-group-text">Passwort</span>
    </div>
    <input type="password" name="login-password" aria-label="Passwort" class="form-control bg-dark text-white">
  </div>
  <br />
  <button style="margin-bottom:10px;" class="btn btn-primary">Anmelden</button>
</form>
<a href="index.php?s=register">Noch kein Konto?</a>