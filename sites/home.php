<div class="row align-items-center justify-content-center h-100">
  <div class="col-md-4">
    <div class="card bg-dark" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">Update 3</h5>
        <h6 class="card-subtitle mb-2 text-muted">Patch 0.1.2.3</h6>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" class="card-link">Card link</a>
        <a href="#" class="card-link">Another link</a>
      </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="card bg-dark" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">Update 2</h5>
        <h6 class="card-subtitle mb-2 text-muted">Patch 0.1.2.2</h6>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" class="card-link">Card link</a>
        <a href="#" class="card-link">Another link</a>
      </div>
    </div>
  </div>
  <div class="col-md-4t">
    <div class="card bg-dark" style="width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">Update 1</h5>
        <h6 class="card-subtitle mb-2 text-muted">Patch 0.1.2.1</h6>
        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
        <a href="#" class="card-link">Card link</a>
        <a href="#" class="card-link">Another link</a>
      </div>
    </div>
  </div>
</div>