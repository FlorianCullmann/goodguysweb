<?php
if(!isset($_GET['s']))
{
  $_GET['s'] = "home";
}

switch($_GET['s'])
{
  case "home":
    include("sites/home.php");
    break;
  
  case "login":
    include("sites/login.php");
    break;

  case "community":
    include("sites/community.php");
    break;

  case "team":
    include("sites/team.php");
    break;

  case "profile":
    include("sites/profile.php");
    break;
  
  case "store":
    include("sites/store.php");
    break;
  
  case "logout":
    unset($_SESSION['ID']);
    header("Location: index.php?s=login&logout=true");
    break;
  default:
    include("sites/404.php");
    break;
}
?>