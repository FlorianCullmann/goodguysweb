<?php
function getRareStatus($itemID)
{
  global $db;
  $query = mysqli_query($db, "SELECT limitation FROM gg_items WHERE ID='".$itemID."'");
  $fetch = mysqli_fetch_object($query);
  
  $l = $fetch->limitation;

  if($l == 1)
  {
    return "einmalig";
  }
  elseif($l <= 10)
  {
    return "extrem limitiert";
  }
  elseif($l <= 50)
  {
    return "stark limitiert";
  }
  elseif($l <= 200)
  {
    return "limitiert";
  }
  elseif($l <= 500)
  {
    return "selten";
  }
  elseif($l <= 1250)
  {
    return "normal";
  }
  elseif($l <= 2500)
  {
    return "häufig";
  }
  elseif($l > 2500)
  {
    return "sehr häufig";
  }
}
?>