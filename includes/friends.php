<?php
function getFriendshipButton($friend1, $friend2)
{
  global $db;

  $query = mysqli_query($db, "SELECT * FROM gg_friends WHERE user1='".$friend1."' AND user2='".$friend2."' OR user1='".$friend2."' AND user2='".$friend1."'");
  if(mysqli_num_rows($query) == 1)
  {
    $fetch = mysqli_fetch_object($query);
    if($fetch->accepted == 0)
    {
      if($fetch->user1 == $friend1)
      {
        $friendlink = '<a href="controller/friends.php?f='.$friend2.'&m=deleteRequest"><button class="btn btn-primary">Anfrage zurückziehen</button></a>';
      }
      else
      {
        $friendlink = '<a href="controller/friends.php?f='.$friend2.'&m=acceptRequest"><button class="btn btn-primary">Anfrage annehmen</button></a>';
      }
    }
    else
    {
      $friendlink = '<a href="controller/friends.php?f='.$friend2.'&m=deleteFriend"><button class="btn btn-primary">Freund entfernen</button></a>';
    }
  }
  else
  {
    $friendlink = '<a href="controller/friends.php?f='.$friend2.'&m=sendRequest"><button class="btn btn-primary">Als Freund hinzufügen</button></a>';
  }
  return $friendlink;
}
?>