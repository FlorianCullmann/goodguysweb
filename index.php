<?php
require_once("includes/config.php");
if(!isset($_GET['s']))
{
  $_GET['s'] = "home";
}
?>

<!doctype html>
<html lang="de">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="assets/bs/css/bootstrap.min.css">

    <!-- GoodGuys CSS -->
    <link rel="stylesheet" href="assets/style.css">

    <title>GoodGuys Gaming Community</title>
  </head>
  <body class="text-white">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <a class="navbar-brand" href="index.php">
          <img src="assets/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="">
          <strong>Good</strong>Guys
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item<?php if($_GET['s'] == "home") { ?> active <?php } ?>">
              <a class="nav-link" href="index.php">Home <?php if($_GET['s'] == "home") { ?><span class="sr-only">(current)</span><?php } ?></a>
            </li>
            <li class="nav-item<?php if($_GET['s'] == "community") { ?> active <?php } ?>">
              <a class="nav-link" href="index.php?s=community">Community <?php if($_GET['s'] == "community") { ?><span class="sr-only">(current)</span><?php } ?></a>
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Server
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="ts3server://jk4w.com?port=9989">Teamspeak3</a>
                <a class="dropdown-item" href="#">Minecraft</a>
              </div>
            </li>
            <li class="nav-item<?php if($_GET['s'] == "team") { ?> active <?php } ?>">
              <a class="nav-link" href="index.php?s=team">Das Team <?php if($_GET['s'] == "team") { ?><span class="sr-only">(current)</span><?php } ?></a>
            </li>
            <li class="nav-item<?php if($_GET['s'] == "stream") { ?> active <?php } ?>">
              <a class="nav-link" href="index.php?s=stream">Stream <?php if($_GET['s'] == "stream") { ?><span class="sr-only">(current)</span><?php } ?></a>
            </li>
            <li class="nav-item<?php if($_GET['s'] == "youtube") { ?> active <?php } ?>">
              <a class="nav-link" href="index.php?s=youtube">YouTube <?php if($_GET['s'] == "youtube") { ?><span class="sr-only">(current)</span><?php } ?></a>
            </li>
            <li class="nav-item<?php if($_GET['s'] == "contact") { ?> active <?php } ?>">
              <a class="nav-link" href="index.php?s=contact">Kontakt <?php if($_GET['s'] == "contact") { ?><span class="sr-only">(current)</span><?php } ?></a>
            </li>
            <?php if(!isset($_SESSION['ID'])) { ?>
              <li class="nav-item<?php if($_GET['s'] == "login") { ?> active <?php } ?>">
                <a class="nav-link" href="index.php?s=login">Anmelden <?php if($_GET['s'] == "login") { ?><span class="sr-only">(current)</span><?php } ?></a>
              </li>
            <?php } else { ?>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-success" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <?= $USER['username']; ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="index.php?s=profile">Mein Profil</a>
                  <a class="dropdown-item" href="index.php?s=logout">Abmelden</a>
                </div>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </nav>

    <?php if(isset($_SESSION['ID'])) { ?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <div class="navbar-collapse collapse" id="navbar2">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item <?php if($_GET['s'] == "ranglisten") { echo 'active'; } ?>">
              <a class="nav-link" href="index.php?s=ranglisten">Ranglisten</a>
            </li>
            <li class="nav-item <?php if($_GET['s'] == "turniere") { echo 'active'; } ?>">
              <a class="nav-link" href="index.php?s=turniere">Turniere</a>
            </li>
            <li class="nav-item <?php if($_GET['s'] == "collection") { echo 'active'; } ?>">
              <a class="nav-link" href="index.php?s=collection">Collection</a>
            </li>
            <li class="nav-item <?php if($_GET['s'] == "store") { echo 'active'; } ?>">
                <a class="nav-link" href="index.php?s=store">Store</a>
              </li>
            <li class="nav-item">
              <a class="nav-link" style="color:yellow;" href="#"><?= $USER['gc']; ?> GC</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <?php } ?>

    <?php if($_GET['s'] == "home") { ?>
        <div id="demo" class="carousel slide" data-ride="carousel">
          <ul class="carousel-indicators">
            <li data-target="#demo" data-slide-to="0" class="active"></li>
            <li data-target="#demo" data-slide-to="1"></li>
            <li data-target="#demo" data-slide-to="2"></li>
          </ul>
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="assets/img/slider1.png" alt="Los Angeles" width="1100" height="500">
              <div class="carousel-caption d-none d-md-block">
                <h3>Los Angeles</h3>
                <p>We had such a great time in LA!</p>
              </div>   
            </div>
            <div class="carousel-item">
              <img src="assets/img/slider2.png" alt="Chicago" width="1100" height="500">
              <div class="carousel-caption d-none d-md-block">
                <h3>Chicago</h3>
                <p>Thank you, Chicago!</p>
              </div>   
            </div>
            <div class="carousel-item">
              <img src="assets/img/slider3.png" alt="New York" width="1100" height="500">
              <div class="carousel-caption d-none d-md-block">
                <h3>New York</h3>
                <p>We love the Big Apple!</p>
              </div>   
            </div>
          </div>
          <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
          </a>
          <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
          </a>
        </div>
      </div>
    <?php } ?>
    
    <div style="margin-top:20px;" id="content" class="container">
      <?php include("sitehandler.php"); ?>
    </div>

    <footer class="footer bg-dark">
      <div class="container" id="footer-container">
        <span class="text-white">&copy; 2018 GoodGuys | powered by <a href="https://blocks25.eu">blocks25.eu</a> | <a href="https://blocks25.eu/datenschutzerklaerung">Datenschutzerklärung</a> | <a href="https://blocks25.eu/impressum">Impressum</a></span>
      </div>
    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="assets/bs/js/bootstrap.min.js"></script>
  </body>
</html>